//
//  AppConstants.swift
//  TestProject
//
//  Created by Oleksandr S. Herasymchuk on 01/28/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import Foundation

struct AppConstants {
    static let commitsURL = "https://gitlab.com/api/v4/projects/10497709/repository/commits"
    static let notificationIdentifier = "NotificationIdentifier"
    static let errorKey = "error"
    static let tableRowHeight = 75.0
}
