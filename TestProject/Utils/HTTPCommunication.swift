//
//  HTTPCommunication.swift
//  TestProject
//
//  Created by Oleksandr S. Herasymchuk on 01/28/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import Foundation

protocol HTTPCommunicationDelegate: AnyObject {
    func didReceiveData(_ data: Data)
    func errorOccurred(_ errorDescription: String)
}

enum RequestType: String {
    case GET = "GET"
    case POST = "POST"
}

class HTTPCommunication {
    
    weak var delegate: HTTPCommunicationDelegate?
    
    func sendHTTPRequest(stringURL: String, parameters: [String : Any]?, requestType: RequestType) {
        var request: URLRequest?
        
        if let parameters = parameters {
            switch requestType {
            case .GET:
                var urlWithParameters = stringURL + "?"
                for (key, value) in parameters {
                    urlWithParameters += key + "=" + (defineType(object: value) ?? "nil") + "&"
                }
                urlWithParameters.removeLast()
                
                if !createRequest(stringURL: urlWithParameters, request: &request, type: requestType) {
                    return
                }
            case .POST:
                if !createRequest(stringURL: stringURL, request: &request, type: requestType) {
                    return
                }
                
                let requestBody: Data
                do {
                    requestBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                    request!.httpBody = requestBody
                } catch {
                    self.delegate?.errorOccurred(error.localizedDescription)
                    return
                }
            }
        } else {
            if !createRequest(stringURL: stringURL, request: &request, type: requestType) {
                return
            }
        }

        let session = URLSession.shared
        session.dataTask(with: request!)
        {
            (data, response, error) in
            if error == nil {
                guard let data = data else {
                    self.delegate?.errorOccurred("Bad data received")
                    return
                }
                self.delegate?.didReceiveData(data)
            } else {
                if let error = error as NSError? {
                    self.delegate?.errorOccurred(error.localizedDescription)
                } else {
                    self.delegate?.errorOccurred("Unexpected error")
                }
            }
        }.resume()
    }
    
    private func defineType(object: Any) -> String? {
        var result: String?
        switch object {
        case let intValue as Int:
            result = String(intValue)
        case let stringValue as String:
            result = stringValue
        case let boolValue as Bool:
            result = String(boolValue)
        case let doubleValue as Double:
            result = String(doubleValue)
        default:
            break
        }
        return result
    }
    
    private func createRequest(stringURL: String, request: inout URLRequest?, type: RequestType) -> Bool {
        if let url = URL(string: stringURL) {
            request = URLRequest(url: url)
            request!.httpMethod = type.rawValue
            return true
        } else {
            self.delegate?.errorOccurred("Can't create URL")
            return false
        }
    }
}
