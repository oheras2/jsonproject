//
//  SegmentedViewController.swift
//  TestProject
//
//  Created by Oleksandr S. Herasymchuk on 02/08/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import UIKit

class SegmentedViewController: UIViewController {
    
    private let mainStoryBoardTitle = "Main"
    private let mapViewControllerIdentifier = "mapViewController"
    private let locationsTableViewControllerIdentifier = "locationsTableViewController"
    
    private lazy var mapViewController: MapViewController = {
        let storyboard = UIStoryboard(name: mainStoryBoardTitle, bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: mapViewControllerIdentifier) as! MapViewController
        return viewController
    }()
    
    private lazy var locationsTableViewController: LocationsListViewController = {
        let storyboard = UIStoryboard(name: mainStoryBoardTitle, bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: locationsTableViewControllerIdentifier) as! LocationsListViewController
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addChildView(locationsTableViewController)
    }
    
    @IBAction func segmentTapped(_ sender: UISegmentedControl) {
        configureView(selectedIndex: sender.selectedSegmentIndex)
    }
    
    private func configureView(selectedIndex: Int) {
        if selectedIndex == 0 {
            removeChildView(mapViewController)
            addChildView(locationsTableViewController)
        } else {
            removeChildView(locationsTableViewController)
            addChildView(mapViewController)
        }
    }
    
    private func addChildView(_ viewController: UIViewController) {
        addChild(viewController)
        view.addSubview(viewController.view)
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    
    private func removeChildView(_ viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    deinit {
        print("SegmentedViewController deinit")
    }
}
