//
//  MapViewController.swift
//  TestProject
//
//  Created by Oleksandr S. Herasymchuk on 02/13/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate {
    
    private let yourCoordinates = "yourCoordinates"
    private let enableLocationServicesKey = "enableLocationServices"
    private let noPermissionKey = "noPermission"
    private let locationManager = CLLocationManager()
    
    @IBOutlet weak var map: MKMapView!
    private var isRequestStarted = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMap()
    }
    
    private func configureMap() {
        if checkPermissions(authorizationStatus: CLLocationManager.authorizationStatus()) {
            if checkLocationServices() {
                requestLocation()
            }
        } else {
            locationManager.delegate = self
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    private func requestLocation() {
        if !isRequestStarted {
            isRequestStarted = true
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestLocation()
        }
    }
    
    private func checkPermissions(authorizationStatus status: CLAuthorizationStatus) -> Bool {
        var result = false
        if status == CLAuthorizationStatus.authorizedWhenInUse || status == CLAuthorizationStatus.authorizedAlways {
            result = true
        }
        return result
    }
    
    private func checkLocationServices() -> Bool {
        var result = false
        if CLLocationManager.locationServicesEnabled() {
            result = true
        } else {
            showError(errorMessage: NSLocalizedString(enableLocationServicesKey, comment: "Enable location services"))
        }
        return result
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status != .notDetermined {
            if checkPermissions(authorizationStatus: status) {
                if checkLocationServices() {
                    requestLocation()
                }
            } else {
                showError(errorMessage: NSLocalizedString(noPermissionKey, comment: "No permission"))
            }
        }
    }
    
    private func showError(errorMessage: String) {
        let alert = UIAlertController(title: NSLocalizedString(AppConstants.errorKey, comment: "Error"), message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let userLocation = locations.last?.coordinate else {
            return
        }
        let viewRegion = MKCoordinateRegion.init(center: userLocation, latitudinalMeters: 100, longitudinalMeters: 100)
        self.map.setRegion(viewRegion, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = userLocation
        annotation.title = NSLocalizedString(yourCoordinates, comment: "Your coordinates")
        self.map.addAnnotation(annotation)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        showError(errorMessage: error.localizedDescription)
    }
    
    deinit {
        print("MapViewController deinit")
    }
}
