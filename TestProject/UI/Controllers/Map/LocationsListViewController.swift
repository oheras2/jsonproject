//
//  LocationsListViewController.swift
//  TestProject
//
//  Created by Oleksandr S. Herasymchuk on 02/13/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import UIKit

class LocationsListViewController: UITableViewController {

    @IBOutlet var locationsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationsTableView.tableFooterView = UIView()
    }

    deinit {
        print("LocationsListViewController deinit")
    }
}
