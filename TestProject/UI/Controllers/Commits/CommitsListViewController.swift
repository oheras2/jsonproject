//
//  CommitsListViewController.swift
//  TestProject
//
//  Created by Oleksandr S. Herasymchuk on 01/23/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import UIKit

class CommitsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CommitsDataManagerDelegate {
    
    private let tableViewCellId = "CellIdentifier"
    private let commitDetailsViewControllerId = "DetailsViewController"
    
    @IBOutlet weak var commitsTableView: UITableView!
    @IBOutlet weak var emptyDataLabel: UILabel!
    @IBOutlet weak var dataProgressSpinner: UIActivityIndicatorView!
    private var commitsArray: [Commit] = []
    private let commitsDataManager: CommitsDataManager = CommitsDataManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.commitsTableView.tableFooterView = UIView()
    }
    
    //UITableView deselect row after back click
    override func viewWillAppear(_ animated: Bool) {
        if let index = self.commitsTableView.indexPathForSelectedRow {
            self.commitsTableView.deselectRow(at: index, animated: true)
        }
    }
    
    @IBAction func loadData() {
        configureDataProgressSpinner(isVisible: true)
        commitsDataManager.delegate = self
        commitsDataManager.loadData(inputURL: AppConstants.commitsURL)
    }
    
    private func configureDataProgressSpinner(isVisible: Bool) {
        if isVisible {
            emptyDataLabel.isHidden = true
            commitsTableView.isHidden = true
            dataProgressSpinner.startAnimating()
        } else {
            dataProgressSpinner.stopAnimating()
        }
    }
    
    private func configureEmptyDataLabel(hasData: Bool) {
        if hasData {
            emptyDataLabel.isHidden = true
            commitsTableView.isHidden = false
        } else {
            emptyDataLabel.isHidden = false
            commitsTableView.isHidden = true
        }
    }
    
    deinit {
        print("CommitsListViewController deinit")
    }
    
    //MARK: - CommitsDataManagerDelegate
    //CommitsDataManagerDelegate method (called when data is ready)
    func dataReady(_ result: [Commit]) {
        commitsArray = result
        DispatchQueue.main.async {
            self.configureDataProgressSpinner(isVisible: false)
            self.commitsTableView.reloadData()
        }
    }
    
    //Error message
    func errorOccurred(_ errorDescription: String) {
        DispatchQueue.main.async {
            self.configureDataProgressSpinner(isVisible: false)
            let alert = UIAlertController(title: NSLocalizedString(AppConstants.errorKey, comment: "Error"), message: errorDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: {
                self.configureEmptyDataLabel(hasData: false)
            })
        }
    }
    
    //MARK: - UITableViewDataSource
    //UITableView rows number method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        configureEmptyDataLabel(hasData: commitsArray.count != 0)
        return commitsArray.count
    }
    
    //UITableView every row cell creation method
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellId, for: indexPath)
        cell.textLabel?.text = commitsArray[indexPath.row].title
        cell.detailTextLabel?.text = commitsArray[indexPath.row].authorName
        return cell
    }
    
    //MARK: - UITableViewDelegate
    //UITableView row height method
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(AppConstants.tableRowHeight)
    }
    
    //UITableView on row click method
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row = \(tableView.cellForRow(at: indexPath)?.textLabel?.text ?? "nil")")
        let detailsViewController = storyboard?.instantiateViewController(withIdentifier: commitDetailsViewControllerId)
        self.navigationController!.pushViewController(detailsViewController!, animated: true)
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Notification.Name(AppConstants.notificationIdentifier),
                                            object: self.commitsArray[indexPath.row])
        }
    }
}
