//
//  CommitDetailsViewController.swift
//  TestProject
//
//  Created by Oleksandr S. Herasymchuk on 01/25/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import UIKit

class CommitDetailsViewController: UIViewController {

    @IBOutlet weak var committedDateLabel: UILabel!
    @IBOutlet weak var committerEmailLabel: UILabel!
    @IBOutlet weak var committerNameLabel: UILabel!
    @IBOutlet weak var authoredLabel: UILabel!
    @IBOutlet weak var authorEmailLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var shortIdLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    private var commit: Commit?
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(receivedNotification(notification:)),
                                               name: Notification.Name(AppConstants.notificationIdentifier), object: nil)
        super.viewDidLoad()
    }
    
    @objc func receivedNotification(notification: Notification) {
        if let receivedCommit = notification.object as? Commit {
            self.commit = receivedCommit
            showDetails()
        }
    }
    
    private func showDetails() {
        idLabel.text = commit?.id
        shortIdLabel.text = commit?.shortID
        titleLabel.text = commit?.title
        timeLabel.text = commit?.createdAt
        messageLabel.text = commit?.message
        authorNameLabel.text = commit?.authorName
        authorEmailLabel.text = commit?.authorEmail
        authoredLabel.text = commit?.authoredDate
        committerNameLabel.text = commit?.committerName
        committerEmailLabel.text = commit?.committerEmail
        committedDateLabel.text = commit?.committedDate
    }

    deinit {
        print("CommitDetailsViewController deinit")
    }
}
