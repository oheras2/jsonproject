//
//  SettingsViewController.swift
//  TestProject
//
//  Created by Oleksandr S. Herasymchuk on 02/08/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController, UserDataDelegate {
    
    private let googleSwitchKey = "google"
    private let yandexSwitchKey = "yandex"
    private let appleSwitchKey = "apple"
    private let gitHubSwitchKey = "gitHub"
    private let gitLabSwitchKey = "gitLab"
    private let bitBucketSwitchKey = "bitBucket"
    private let userNameKey = "userName"
    private let isUserLoggedKey = "isUserLogged"
    private let logInKey = "logIn"
    private let logOutKey = "logOut"
    private let userNotLoggedKey = "notLogged"
    private let userDefaults = UserDefaults.standard
    
    private var isUserLogged = false
    
    @IBOutlet var settingsTableView: UITableView!
    @IBOutlet weak var googleSwitch: UISwitch!
    @IBOutlet weak var yandexSwitch: UISwitch!
    @IBOutlet weak var appleSwitch: UISwitch!
    @IBOutlet weak var gitHubSwitch: UISwitch!
    @IBOutlet weak var gitLabSwitch: UISwitch!
    @IBOutlet weak var bitBucketSwitch: UISwitch!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userAccountButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.settingsTableView.tableFooterView = UIView()
        self.settingsTableView.allowsSelection = false
        self.settingsTableView.contentInset = UIEdgeInsets(top: UIApplication.shared.statusBarFrame.height, left: 0, bottom: 0, right: 0)
        loadUserData()
        (UIApplication.shared.delegate as! AppDelegate).delegate = self
    }
    
    func saveUserData() {
        userDefaults.set(googleSwitch.isOn, forKey: googleSwitchKey)
        userDefaults.set(yandexSwitch.isOn, forKey: yandexSwitchKey)
        userDefaults.set(appleSwitch.isOn, forKey: appleSwitchKey)
        userDefaults.set(gitHubSwitch.isOn, forKey: gitHubSwitchKey)
        userDefaults.set(gitLabSwitch.isOn, forKey: gitLabSwitchKey)
        userDefaults.set(bitBucketSwitch.isOn, forKey: bitBucketSwitchKey)
        userDefaults.set(isUserLogged, forKey: isUserLoggedKey)
        if isUserLogged {
            userDefaults.setValue(userNameLabel.text, forKey: userNameKey)
        }
    }
    
    private func loadUserData() {
        loadSwitchUserData(switchObject: &googleSwitch, key: googleSwitchKey)
        loadSwitchUserData(switchObject: &yandexSwitch, key: yandexSwitchKey)
        loadSwitchUserData(switchObject: &appleSwitch, key: appleSwitchKey)
        loadSwitchUserData(switchObject: &gitHubSwitch, key: gitHubSwitchKey)
        loadSwitchUserData(switchObject: &gitLabSwitch, key: gitLabSwitchKey)
        loadSwitchUserData(switchObject: &bitBucketSwitch, key: bitBucketSwitchKey)
        loadUserName()
    }
    
    private func loadUserName() {
        if let isUserLoggedIn = userDefaults.object(forKey: isUserLoggedKey) as? Bool {
            isUserLogged = isUserLoggedIn
            if isUserLoggedIn {
                if let userName = userDefaults.object(forKey: userNameKey) as? String {
                    userNameLabel.text = userName
                    configureButton()
                }
            }
        }
    }
    
    private func configureButton() {
        if isUserLogged {
            userAccountButton.setTitle(NSLocalizedString(logOutKey, comment: "Log out button title"), for: .normal)
            userAccountButton.setTitleColor(UIColor.red, for: .normal)
        } else {
            userAccountButton.setTitle(NSLocalizedString(logInKey, comment: "Log in button title"), for: .normal)
            userAccountButton.setTitleColor(UIColor.green, for: .normal)
        }
    }
    
    private func loadSwitchUserData(switchObject: inout UISwitch, key: String) {
        if let isOn = userDefaults.object(forKey: key) {
            switchObject.isOn = isOn as! Bool
        }
    }
    
    @IBAction func loginUser() {
        configureUser()
    }
    
    private func configureUser() {
        if isUserLogged {
            userNameLabel.text = NSLocalizedString(userNotLoggedKey, comment: "User not logged in")
            isUserLogged = !isUserLogged
            configureButton()
        } else {
            let alert = UIAlertController(title: NSLocalizedString(logInKey, comment: "Log in alert"), message: NSLocalizedString(userNameKey, comment: "Enter user name"), preferredStyle: .alert)
            alert.addTextField(configurationHandler: nil)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                guard let user = alert!.textFields![0].text else {
                    return
                }
                if !user.isEmpty {
                    self.isUserLogged = !self.isUserLogged
                    self.userNameLabel.text = user
                    self.configureButton()
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    deinit {
        print("SettingsViewController deinit")
    }
}
