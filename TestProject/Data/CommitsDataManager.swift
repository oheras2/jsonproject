//
//  CommitsDataManager.swift
//  TestProject
//
//  Created by Oleksandr S. Herasymchuk on 01/28/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import Foundation

protocol CommitsDataManagerDelegate: AnyObject {
    func dataReady(_ result: [Commit])
    func errorOccurred(_ errorDescription: String)
}

class CommitsDataManager: HTTPCommunicationDelegate {
    
    weak var delegate: CommitsDataManagerDelegate?
    
    func didReceiveData(_ data: Data) {
        var result: [Commit] = []
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options: []) as? [[String : AnyObject]] {
                for jsonItem in jsonArray {
                    result.append(Commit(JSON: jsonItem))
                }
            }
        } catch {
            self.delegate?.errorOccurred(error.localizedDescription)
            return
        }
        delegate?.dataReady(result)
    }
    
    func errorOccurred(_ errorDescription: String) {
        self.delegate?.errorOccurred(errorDescription)
    }
    
    func loadData(inputURL: String) {
        makeRequest(inputURL: inputURL, inputParameters: nil, requestType: .GET)
    }
    
    func loadData(inputURL: String, inputParameters: [String : Any]) {
        makeRequest(inputURL: inputURL, inputParameters: inputParameters, requestType: .GET)
    }
    
    func loadData(inputURL: String, inputParameters: [String : Any]?, requestType: RequestType) {
        makeRequest(inputURL: inputURL, inputParameters: inputParameters, requestType: requestType)
    }
    
    private func makeRequest(inputURL: String, inputParameters: [String : Any]?, requestType: RequestType) {
        let httpRequest = HTTPCommunication()
        httpRequest.delegate = self
        httpRequest.sendHTTPRequest(stringURL: inputURL, parameters: inputParameters, requestType: requestType)
    }
}
