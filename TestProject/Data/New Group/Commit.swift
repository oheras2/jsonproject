//
//  Commit.swift
//  TestProject
//
//  Created by Oleksandr S. Herasymchuk on 01/25/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import Foundation

struct Commit {
    let authorEmail: String
    let authorName: String
    let authoredDate: String
    let committedDate: String
    let committerEmail: String
    let committerName: String
    let createdAt: String
    let id: String
    let message: String
    let shortID: String
    let title: String
    
    init(JSON: [String : AnyObject]) {
        authorEmail = JSON["author_email"] as? String ?? ""
        authorName = JSON["author_name"] as? String ?? ""
        authoredDate = JSON["authored_date"] as? String ?? ""
        committedDate = JSON["committed_date"] as? String ?? ""
        committerEmail = JSON["committer_email"] as? String ?? ""
        committerName = JSON["committer_name"] as? String ?? ""
        createdAt = JSON["created_at"] as? String ?? ""
        id = JSON["id"] as? String ?? ""
        message = String((JSON["message"] as? String ?? "").filter { !"\n".contains($0) })
        shortID = JSON["short_id"] as? String ?? ""
        title = JSON["title"] as? String ?? ""
    }
}
